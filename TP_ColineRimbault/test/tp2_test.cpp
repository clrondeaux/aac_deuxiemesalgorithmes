// Entetes //---------------------------------------------------------------------------------------
#include "catch.hpp"
#include <cmath>
#include <arbre.hpp>


TEST_CASE ( "Un ancetre commun" )
{
    Arbre arbre = Arbre();
    vector<int> prec = {0, 0, 0, 1, 2, 2 , 3};
    arbre.__init__(prec);
    int res = arbre.requete(4, 5);
    REQUIRE(res == 2) ;
}

TEST_CASE ( "Aucun ancetre commun" )
{
    Arbre arbre = Arbre();
    vector<int> prec = {0, 0, 0, 1, 2, 2 , 3};
    arbre.__init__(prec);
    int res = arbre.requete(3, 5);
    REQUIRE(res == 0) ;
}

TEST_CASE ( "Plusieurs couple de parents possible pas interchangé" )
{
    Arbre arbre = Arbre();
    vector<int> prec = {0, 0, 0, 1, 1 , 2, 2};
    arbre.__init__(prec);
    int res = arbre.requete(3, 4);
    REQUIRE(res == 1) ;
    res = arbre.requete(5, 6);
    REQUIRE(res == 2) ;
}

TEST_CASE ( "Le plus petit arbre possible" )
{
    Arbre arbre = Arbre();
    vector<int> prec = {0 , 0 , 0};
    arbre.__init__(prec);
    int res = arbre.requete(1 , 2);
    REQUIRE(res == 0) ;
}
