/* -------------------------------------------------------------------------- */
/**/
/* -------------------------------------------------------------------------- */
// ligne de compilation g++ anagram.cpp -o exe -Wall
#include <sstream>
#include <iterator>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <fstream>
#include <cmath>
#include <cassert>
using namespace std;

#include "arbre.hpp"

// pour le calcul de la profondeur de l'arbre
int Arbre::calcProfondeur()
{
    int profondeur = *max_element(this->niveau.begin(), this->niveau.end());
    return std::ceil(std::log2(profondeur)) + 1;
}

void Arbre::affichage()
{
    cout << "{";
    for (auto const &s : niveau)
    {
        cout << s << " - ";
    }
    cout << "}\n";

    cout << "{";
    for (auto const &it : ancetre)
    {
        cout << "{";
        for (auto const &s : it)
        {
            cout << s << " - ";
        }
        cout << "}\n";
    }
    cout << "}\n";

    cout << profondeur << "\n";
}

// en entrée le tableau prec, pour le noeud précédent le noeud courant
void Arbre::__init__(vector<int> prec)
{
    vector<int> tmp;
    int n = prec.size();
    this->niveau.resize(n, 0);
    // ajout de l'information des niveaux à l'arbre
    this->niveau[0] = 0;
    for (int u = 1; u < n; u++)
    {
        this->niveau[u] = 1 + this->niveau[prec[u]];
    }
    this->profondeur = calcProfondeur();
    // ajout de l'information des ancêtres
    this->ancetre.resize(this->profondeur, vector<int>(n, 0));
    for (int u = 0; u < n; u++)
    {
        tmp.push_back(prec[u]);
    }
    this->ancetre[0] = tmp;
    for (int k = 1; k < this->profondeur; k++)
    {
        for (int u = 0; u < n; u++)
        {
            this->ancetre[k][u] = this->ancetre[k - 1][this->ancetre[k - 1][u]];
        }
    }
}

// on fait une "requete" en donnant deux noeud, pour connaître leur ancètre commun
int Arbre::requete(int u, int v)
{
    // on suppose que v n'est pas plus haut que u dans l'arbre
    if (this->niveau[u] > this->niveau[v])
    {
        swap(u, v);
    }
    this->profondeur = this->ancetre.size();
    for (int k = this->profondeur - 1; k >= 0; k--)
    {
        if (this->niveau[u] <= this->niveau[v] - (1 << k))
        {
            v = this->ancetre[k][v];
        }
    }
    assert(this->niveau[u] == this->niveau[v]);
    if (u == v)
    {
        return u;
    }
    // remonter jusqu'à l'ancètre commun le plus proche
    for (int k = this->profondeur - 1; k >= 0; k--)
    {
        if (this->ancetre[k][u] != this->ancetre[k][v])
        {
            u = this->ancetre[k][u];
            v = this->ancetre[k][v];
        }
    }
    assert(this->ancetre[0][u] == this->ancetre[0][v]);
    return this->ancetre[0][u];
}

Arbre::Arbre()
{
    profondeur = 0;
}