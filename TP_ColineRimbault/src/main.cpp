#include <iostream>
#include "arbre.hpp"

// ligne de compilation g++ anagram.cpp -o exe -Wall

using namespace std;

int main()
{
    Arbre arbre = Arbre();
    vector<int> prec = {0, 0, 0, 1, 1, 2, 2};
    arbre.__init__(prec);
    arbre.affichage();
    int res = arbre.requete(4, 5);
    cout << res;
    return 0;
}