#ifndef AAC_ARBRE
#define AAC_ARBRE

#include <vector>
#include <string>
using namespace std;

class Arbre
{
public:
    Arbre();
    void affichage();
    int calcProfondeur();
    void __init__(vector<int>);
    int requete(int, int);

private:
    vector<int> niveau;
    vector<vector<int>> ancetre;
    int profondeur;
};

#endif