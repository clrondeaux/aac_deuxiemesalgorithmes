using ExpreArithmetique.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace EvalExpTest
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void EvalCalc_Addition_ReturnsCorrectResult()
        {
            // Arrange
            var evalArithmetique = new EvalArithmetique();
            var input = new List<string> { "5", "+", "3" };
            var expected = 8;

            // Act
            var actual = evalArithmetique.EvalCalc(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void EvalCalc_Soustraction_ReturnsCorrectResult()
        {
            // Arrange
            var evalArithmetique = new EvalArithmetique();
            var input = new List<string> { "9", "-", "4" };
            var expected = 5;

            // Act
            var actual = evalArithmetique.EvalCalc(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void EvalCalc_Multiplication_ReturnsCorrectResult()
        {
            // Arrange
            var evalArithmetique = new EvalArithmetique();
            var input = new List<string> { "3", "*", "7" };
            var expected = 21;

            // Act
            var actual = evalArithmetique.EvalCalc(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void EvalCalc_Division_ReturnsCorrectResult()
        {
            // Arrange
            var evalArithmetique = new EvalArithmetique();
            var input = new List<string> { "10", "/", "2" };
            var expected = 5;

            // Act
            var actual = evalArithmetique.EvalCalc(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void EvalCalc_AdditionMultiplication_ReturnsCorrectResult()
        {
            // Arrange
            var evalArithmetique = new EvalArithmetique();
            var input = new List<string> { "2", "+", "3", "*", "4" };
            var expected = 14;

            // Act
            var actual = evalArithmetique.EvalCalc(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void EvalCalc_ComplexExpression_ReturnsCorrectResult()
        {
            // Arrange
            var evalArithmetique = new EvalArithmetique();
            var input = new List<string> {"3", "+", "8", "*", "4", "/", "2", "-", "1" };
            var expected = 18;

            // Act
            var actual = evalArithmetique.EvalCalc(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
