﻿namespace ExpreArithmetique.Services
{
    public class EvalArithmetique
    {
        /// <summary>
        /// Liste relistant les priorités d'un opérateur par rapport à l'autre
        /// </summary>
        static readonly Dictionary<string, int> Priority = new Dictionary<string, int>
            {
                {";" , 0 },
                {"(" , 1 },
                {")" , 2 },
                {"+" , 3 },
                {"-" , 3 },
                {"*" , 4 },
                {"/" , 4 },
            };

        /// <summary>
        /// Pile des valeurs atendant d'être calculés
        /// </summary>
        public readonly List<double> Values = new List<double>();

        /// <summary>
        /// Pile des opérateurs attenant d'être calculés
        /// </summary>
        public readonly List<string> Operators = new List<string>();

        /// <summary>
        /// Fonction permettant d'afficher la liste courante des valeurs
        /// </summary>
        private void PrintListValues()
        {
            Console.Write("Values= ");
            foreach (var i in Values)
                Console.Write("{0} " , i.ToString());
            Console.WriteLine();
        }

        /// <summary>
        /// Fonction permettant d'afficher la liste courante des valeurs
        /// </summary>
        private void PrintListOperators()
        {
            Console.Write("Operator= ");
            foreach (var i in Operators)
                Console.Write("{0} " , i.ToString());
            Console.WriteLine();
        }

        /// <summary>
        /// Fonction qui transforme un opérateur en text et qui calcul le résultat
        /// </summary>
        /// <param name="v1">valeur de gauche</param>
        /// <param name="oper">opérateur</param>
        /// <param name="v2">valeur de droite</param>
        /// <returns></returns>
        private double StepEval( double v1 , string oper , double v2 )
        {
            switch(oper)
            {
                case "+":
                    return v1+v2 ;

                case "-":   
                    return v1-v2;

                case "*":
                    return v1*v2;

                case "/":
                    return v1/v2 ;
            }
            return 0;
        }

        /// <summary>
        /// Fonction permettant d'évaluer une liste de charactère pour en calculer le résultat
        /// </summary>
        /// <param name="input">Liste de chaque caractères représentant le calcul</param>
        /// <returns>Le résultat du calcul</returns>
        public double EvalCalc(List<string> input)
        {
            int i = 0;
            int prioPrec , prioCour ;
            double calc;

            string charC ;
            while (i < input.Count)
            {
                charC = input[i];
                i++;

                // Si charC est conetnue dans le dictionnaire priorité, c'est que c'est un opérateur obligatoirement
                if (Priority.ContainsKey(charC))
                    Operators.Add(charC);
                else
                {
                    // Sinon c'est un nombre et on l'ajoute par défaut à notre liste de valeurs
                    Values.Add(Convert.ToDouble(charC));

                    // Dans le cas où on a encore plusieurs opérateurs qui attendent d'être traité
                    if (Operators.Count >= 2)
                    {
                        Priority.TryGetValue(Operators[Operators.Count - 1], out prioCour);     // On récupère l opérateur courant
                        Priority.TryGetValue(Operators[Operators.Count - 2], out prioPrec);     // On récupère l opérateur précédent

                        while (Operators.Count >= 2 && prioCour >= prioPrec)
                        {
                            calc = StepEval(Values[Values.Count - 2], Operators[Operators.Count - 1], Values[Values.Count - 1]);
                            Console.WriteLine("{0} {1} {2} = {3}", Values[Values.Count - 2], Operators[Operators.Count - 1], Values[Values.Count - 1], calc);

                            Operators.RemoveAt(Operators.Count - 1);
                            Values.RemoveAt(Values.Count - 1);
                            Values.RemoveAt(Values.Count - 1);
                            Values.Add(calc);

                            if (Operators.Count >= 2)
                            {
                                Priority.TryGetValue(Operators[Operators.Count - 1], out prioCour);
                                Priority.TryGetValue(Operators[Operators.Count - 2], out prioPrec);
                            }

                            PrintListValues();
                            PrintListOperators();
                        }
                    }
                }
                PrintListValues();
                PrintListOperators();
                Console.WriteLine("-----");
            }

            return StepEval(Values[Values.Count - 2], Operators[Operators.Count - 1], Values[Values.Count - 1]);
        }
    }
}
